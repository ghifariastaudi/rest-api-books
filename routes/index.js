const router = require('express').Router()
const Books = require('../controllers/booksController')
const Admin = require('../controllers/admin/booksController')

// API server
router.post('/api/books', Books.createBooks)
router.get('/api/books', Books.findBooks)
router.get('/api/books/:id', Books.findBooksById)
router.put('/api/books/:id', Books.updateBooks)
router.delete('/api/books/:id', Books.deleteBooks)

// admin client side
router.get('/admin', Admin.homepage)
router.get('/admin/books', Admin.dataPage)
router.get('/admin/new', Admin.createPage)
router.post('/admin/new', Admin.createBooks)
router.get('/admin/edit/:id', Admin.editPage)
router.post('/admin/update/:id', Admin.editBooks)
router.post('/admin/delete/:id', Admin.deleteBooks)

module.exports = router