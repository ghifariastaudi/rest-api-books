const { Books } = require('../models')

const createBooks = async(req, res) => {
    const { judul, penulis, tahun } = req.body
        // req.body.name, req.body.price, req.body.quantity
    try {
        const newBooks = await Books.create({
            judul,
            penulis,
            tahun
        })

        res.status(201).json({
            status: 'success',
            data: {
                newBooks
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findBooks = async(req, res) => {
    try {
        const books = await Books.findAll()
        res.status(200).json({
            status: 'Success',
            data: {
                books
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findBooksById = async(req, res) => {
    try {
        const books = await Books.findOne({
            where: {
                id: req.params.id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                books
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const updateBooks = async(req, res) => {
    try {
        const { judul, penulis, tahun } = req.body
        const id = req.params.id
        const books = await Books.update({
            judul,
            penulis,
            tahun
        }, {
            where: {
                id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                id,
                judul,
                penulis,
                tahun
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const deleteBooks = async(req, res) => {
    try {
        const id = req.params.id
        await Books.destroy({
            where: {
                id
            }
        })

        res.status(200).json({
            status: 'success',
            message: `Books dengan id ${id} terhapus`
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

module.exports = {
    createBooks,
    findBooks,
    findBooksById,
    updateBooks,
    deleteBooks,
}