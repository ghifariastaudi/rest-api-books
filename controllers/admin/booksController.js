const { Books } = require('../../models')
    // const axios = require('axios')

const homepage = async(req, res) => {
    res.render('index')
}

const dataPage = async(req, res) => {
    // const products = await axios.get('localhost:3000/api/product')
    // console.log(products.data)
    const items = await Books.findAll()
    res.render('data', {
        items
    })
}

const createPage = async(req, res) => {
    res.render('new')
}

const createBooks = async(req, res) => {
    const { judul, penulis, tahun } = req.body
        // req.body.name, req.body.price, req.body.quantity
    const newBooks = await Books.create({
        judul,
        penulis,
        tahun
    })
    res.redirect('/admin/books')
}

const editPage = async(req, res) => {
    const item = await Books.findByPk(req.params.id)
    res.render('edit', {
        item
    })
}

const editBooks = async(req, res) => {
    const { judul, penulis, tahun } = req.body
    const id = req.params.id
    await Books.update({
        judul,
        penulis,
        tahun
    }, {
        where: {
            id
        }
    })
    res.redirect('/admin/books')
}

const deleteBooks = async(req, res) => {
    const id = req.params.id
    await Books.destroy({
        where: {
            id
        }
    })
    res.redirect('/admin/books')
}

module.exports = {
    homepage,
    dataPage,
    createPage,
    createBooks,
    editPage,
    editBooks,
    deleteBooks,
}